# A simple demo of node.js with canvas to video
A simple demo of node.js with canvas to video recovered from the presentation of [@rauchg](http://twitter.com/rauchg "twitter") from #nodefest.

# plus
Added cluster and setup file

# Installation

    > git clone git://github.com/alejandromg/nvideo.git
    > cd nvideo
    > ./setup
    # If the setup is ok:
    > node server

# To come:

A really deep implementation of this demo for using with a textarea and record every keyStroke with sound if it's posible, stay tune.

# Contribution

- [Alejandro Morales](http://github.com/alejadromg)
- [Guillermo Rauch](http://github.com/rauchg)

